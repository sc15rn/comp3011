from django.contrib.auth.models import User, Group
from .models import Aircraft, Airport, Flight, Passenger, Booking, PaymentProvider, Invoice
from rest_framework import serializers

# serializers
class FindFlightGet(serializers.ModelSerializer):
    is_flex = serializers.BooleanField()
    num_passengers = serializers.IntegerField()
    departure_datetime = serializers.DateField()
    destination_airport = serializers.CharField()
    departure_airport = serializers.CharField()
    class Meta:
        model = Flight
        fields = ('departure_airport', 'destination_airport', 'departure_datetime', 'num_passengers', 'is_flex')

class FindFlightGetResponse(serializers.ModelSerializer):
    flight_id = serializers.CharField()
    flight_number = serializers.CharField()
    departure_airport = serializers.CharField()
    destination_airport = serializers.CharField()
    departure_datetime = serializers.DateTimeField()
    arrival_datetime = serializers.DateTimeField()
    flight_duration = serializers.DurationField()
    seat_price = serializers.DecimalField(max_digits=6, decimal_places=2)
    class Meta:
        model = Flight
        fields = ('flight_id' , 'flight_number', 'departure_airport', 'destination_airport', 'departure_datetime', 'arrival_datetime', 'flight_duration', 'seat_price')

class PassengerSerializer(serializers.ModelSerializer):
    class Meta:
        model = Passenger
        fields = ('__all__')

class FindFlightPost(serializers.ModelSerializer):
    flight_id = serializers.CharField()
    passengers = PassengerSerializer(many=True)

    class Meta:
        model = Booking
        fields = ('passengers', 'flight_id')

class PaymentProvidersGet(serializers.ModelSerializer):
    #PaymentProvider_id = serializers.IntegerField(read_only=False)

    class Meta:
        model = PaymentProvider
        fields = ('id', 'service_provider_name')
        #fields = ('__all__')

class PaymentForBooking(serializers.ModelSerializer):
    class Meta:
        model = Invoice
        fields = ('booking_number', 'provider_reference_number')


class finalizebookingSerializer(serializers.Serializer):
    booking_number = serializers.CharField()
    provider_reference_number = serializers.IntegerField()
    stamp = serializers.CharField()

class bookingNumSerializer(serializers.Serializer):
    booking_number = serializers.CharField()
    

