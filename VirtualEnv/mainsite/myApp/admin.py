from django.contrib import admin
#Make imports to database tables here
from .models import Aircraft, Airport, Flight, Passenger, Booking, PaymentProvider, Invoice

# Register your models here.
#admin.site.register(book)
admin.site.register(Aircraft)
admin.site.register(Airport)
admin.site.register(Flight)
admin.site.register(Passenger)
admin.site.register(Booking)
admin.site.register(PaymentProvider)
admin.site.register(Invoice)
