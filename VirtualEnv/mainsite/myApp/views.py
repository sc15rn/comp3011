from django.shortcuts import render
from django.contrib.auth.models import User, Group
from rest_framework import viewsets
from rest_framework.views import APIView
from rest_framework.response import Response
from myApp.serializers import FindFlightGet, FindFlightGetResponse, FindFlightPost, PassengerSerializer, PaymentProvidersGet, PaymentForBooking, finalizebookingSerializer, bookingNumSerializer
from .models import Aircraft, Airport, Flight, Passenger, Booking, PaymentProvider, Invoice
from rest_framework import generics
from rest_framework import status
from django.core import serializers
from django.core.exceptions import ObjectDoesNotExist
from django.shortcuts import get_object_or_404
from django.utils.dateparse import parse_datetime
import datetime
import simplejson
from rest_framework.renderers import JSONRenderer
from rest_framework.decorators import api_view
import collections
import random 
import string

def allowed_methods(self):
    """
    Return the list of allowed HTTP methods, uppercased.
    """
    self.http_method_names.append("post")
    return [method.upper() for method in self.http_method_names
            if hasattr(self, method)]


# curl -H 'Content-Type: application/json' -X GET -d '{"departure_airport":"Heathrow", "destination_airport":"Heathrow", "departure_datetime":"2018-03-08", "num_passengers":"1", "is_flex":"0"}' http://127.0.0.1:8000/api/findflight/
# 1) Find Flight - GET
class findflight(APIView):

    #Get method logic for '/api/findflight/'
    def get(self, request):
        serializer = FindFlightGet(data=request.data)

        # Check if the request data is seralizable
        if serializer.is_valid():
            # Retreive the seralised data
            departureAirport = serializer.data['departure_airport']
            destinationAirport = serializer.data['destination_airport']
            departureDate = serializer.data['departure_datetime']
            numberOfPassengers = serializer.data['num_passengers']

            # Do is flex check
            if not serializer.data['is_flex']:
                # Flex = False  -- Departure date is exact

                # query to filter results for the given json payload
                NonFlexQuerySet = Flight.objects.filter(departure_airport=departureAirport, destination_airport=destinationAirport, departure_datetime__date=departureDate )
                # Check if query empty = no results = return 503 message
                if not NonFlexQuerySet:
                    return Response("The search query returned no flights", status=status.HTTP_503_SERVICE_UNAVAILABLE)

                # serlialises the queryset we generated above
                resultsSerializer = FindFlightGetResponse(NonFlexQuerySet, many=True)

                # create a response, sends the json payload
                return Response({'flights':resultsSerializer.data})
            else:
                # Flex = True -- Depature data is flexable

                # Query to filter results for the given json payload
                FlexQuerySet = Flight.objects.filter(departure_airport=departureAirport, destination_airport=destinationAirport)
                # Check if query empty = no results = return 503 message
                if not FlexQuerySet:
                    return Response("The search query returned no flights", status=status.HTTP_503_SERVICE_UNAVAILABLE)

                # Serlialises the queryset we generated above
                resultsSerializer = FindFlightGetResponse(FlexQuerySet, many=True)

                # create a response, sends the json payload
                return Response({'flights':resultsSerializer.data})

        else:
            #serializer is valid check = false
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)





# curl -H 'Content-Type: application/json' -X POST -d '{"flight_id":"1", "passengers":[{"first_name":"Ryan", "surname":"Nelson", "email":"sc15rn@mail.com", "phone_number":"07728776"},{"first_name":"yan", "surname":"Nson", "email":"sc1rn@mail.com", "phone_number":"07728476"}]}' http://127.0.0.1:8000/api/bookflight/
# 2) Book Flight - POST
@api_view(['POST'])
def bookflight(request):

    #Creating new instance in the Booking table
    # Passes JSON payload to the serializer
    serializer = FindFlightPost(data=request.data)
    # Check if the request data is seralizable  
    if serializer.is_valid():
        
        # query database for the flight
        TheFlight = Flight.objects.filter(flight_id = serializer.data['flight_id']).first()  #Get flight object
        # Get the passenger array
        ThePassengers = serializer.data['passengers']
        print(serializer.data['passengers'])

        #Generate 10 digit random booking number
        RandomBookingNumber = ''.join(random.choices(string.ascii_uppercase + string.digits, k=10))
        # set the status to on hold
        TheStatus = 'ON_HOLD'
        # The time held field
        TheHoldTime = "2018-08-08 05:00:00"

        # Array to store passenger object
        objectArray = []                  
        personcount = 0
        for person in ThePassengers:        # Get the ordereddict objects
            personcount = (personcount + 1)   # Increment the person count
            # Create a passenger object
            PassengerObject = Passenger.objects.create(first_name=person.pop('first_name'), surname=person.pop('surname'), email=person.pop('email'), phone_number=person.pop('phone_number'))
            # Append the passenger object to the object array
            objectArray.append(PassengerObject)

        # Create booking object
        BookingObject = Booking.objects.create(booking_number=RandomBookingNumber, flight=TheFlight, booked_seats=personcount, status=TheStatus, hold_time=TheHoldTime)
        # Pass the newly created booking object the object array that contains the passengers
        BookingObject.passenger.set(objectArray)
        # save the booking object
        BookingObject.save()
        # Calculate the total cost of the tickets
        thecost = TheFlight.seat_price * personcount
        print(personcount)
        # return response with created response
        return Response({'booking_number':RandomBookingNumber, 'status':TheStatus, 'tot_price':thecost}, status=status.HTTP_201_CREATED)

    else:
        print("Serializer ERROR")   # Error in serializing
        print(serializer.errors)    # Print out the given errors
        # Return a 400 bad request
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    
# curl -H 'Content-Type: application/json' -X GET http://127.0.0.1:8000/api/paymentmethods/
# 3) Payment Methods - GET
@api_view(['GET'])
def paymentmethods(request):

    # Get all the providers
    theProviders = PaymentProvider.objects.all()
    if not theProviders:
        return Response("The search query returned no payment providers", status=status.HTTP_503_SERVICE_UNAVAILABLE)
        # logic check to see if query is empty ^

    # Pass the providers array to the seraliser
    PaymentProvidersSerializer = PaymentProvidersGet(theProviders, many=True)

    # store the json response
    providerDataArray = PaymentProvidersSerializer.data

    # Pass back to client
    return Response({'payment_providers':providerDataArray}, status=status.HTTP_200_OK)


# curl -H 'Content-Type: application/json' -X POST -d '{"booking_number":"XPP13AEAMP", "provider_reference_number":1}' http://127.0.0.1:8000/api/payforbooking/
# 4) Pay for booking - GET
@api_view(['POST'])
def payforbooking(request):

    # serializer passed the request data
    serializer = PaymentForBooking(data=request.data)
    # Check to see if seralizer is valid
    if serializer.is_valid():
        print("valid")

        # Store elements from request
        TheBookingNumber = serializer.data['booking_number']
        TheProviderRef = serializer.data['provider_reference_number'] 

        # retrieve the booking object
        Thebook = Booking.objects.filter(booking_number=TheBookingNumber).first()
        if not Thebook: # Check if the query is empty
            return Response("Booking number not found", status=status.HTTP_503_SERVICE_UNAVAILABLE)
        
        # Calculate the cost
        Cost = Thebook.flight.seat_price * 2

        # Create invoice object
        theObject = Invoice.objects.create(airline_reference_number=TheProviderRef, provider_reference_number=TheProviderRef, booking_number=TheBookingNumber, amount=Cost, payment_status=True, electronic_stamp=0)

        # Get payment provider object
        ThePaymentProvider = PaymentProvider.objects.filter(id=TheProviderRef).first()

        # retrieve the payment providers website
        Thewebsite = ThePaymentProvider.website

        # Return response
        return Response({'provider_reference_number':TheProviderRef, 'invoice_id':theObject.id, 'booking_number':TheBookingNumber, 'url':Thewebsite}, status=status.HTTP_201_CREATED)
    else:
        # If seralises detects invalid input, trigger error
        print("Error")
        print(serializer.errors)
        return Response("The JSON payload is not valid", status=status.HTTP_503_SERVICE_UNAVAILABLE)


# curl -H 'Content-Type: application/json' -X POST -d '{"booking_number":"XPP13AEAMP", "provider_reference_number": "1", "stamp":"1ew"}' http://127.0.0.1:8000/api/finalizebooking/
# 5) Finalize booking - POST
@api_view(['POST'])
def finalizebooking(request):

    # serialize the request data
    serializer = finalizebookingSerializer(data=request.data)

    # Check if request data is valid
    if serializer.is_valid():
        print("valid")

        # Retrieve the data request elements
        TheBookingNumber = serializer.data['booking_number']
        TheProviderRef = serializer.data['provider_reference_number']
        stamp = serializer.data['stamp']

        # Retrieve a given booking object
        ThebookingObj = Booking.objects.filter(booking_number=TheBookingNumber).first()
        # update it's status
        ThebookingObj.status = "CONFIRMED"
        # commit the change and save
        ThebookingObj.save()

        # Get invoice object
        invoiceObj = Invoice.objects.filter(booking_number=TheBookingNumber).first()
        # Set the invoices object stamp to the one specified by the user
        invoiceObj.electronic_stamp = stamp
        # Save and commit chnages to the object
        invoiceObj.save()

        # Return response
        return Response({'booking_number':TheBookingNumber, 'booking_status':ThebookingObj.status}, status=status.HTTP_201_CREATED)
    else:
        # serialiser didn't except the payload, error
        print("ERROR")
        print(serializer.errors)
        return Response("The JSON payload is not valid", status=status.HTTP_503_SERVICE_UNAVAILABLE)


# curl -H 'Content-Type: application/json' -X POST -d '{"booking_number":"XPP13AEAMP"}' http://127.0.0.1:8000/api/finalizebooking/
# 6) Booking Status - POST
@api_view(['POST'])
def bookingstatus(request):

    # Serialise the request data
    TheSerlizer = bookingNumSerializer(data=request.data)

    # Check to see if it's valid
    if TheSerlizer.is_valid():
        print("Valid stuff")

        # Retreive request data elements
        TheBookingNum = TheSerlizer.data['booking_number']
        # Retrieve booking object
        ThebookingObj = Booking.objects.filter(booking_number=TheBookingNum).first()
        # Retrieve the objects status
        TheBookingStatus = ThebookingObj.status
        # Get the flight number
        FlightNum = ThebookingObj.flight.flight_id
        # get the departure airport
        DepAir = ThebookingObj.flight.departure_airport
        # Get the destiniation airport
        DestAir = ThebookingObj.flight.destination_airport
        # get the depature date time
        DepDateTime = ThebookingObj.flight.departure_datetime
        # get the arival date time
        ArivalDateTime = ThebookingObj.flight.arrival_datetime
        # get the flight duration
        Flightdur = ThebookingObj.flight.flight_duration

        return Response({'booking_number':TheBookingNum, 'booking_status':TheBookingStatus, 'flight_number':FlightNum, 'departure_airport':DepAir, 'destination_airport':DestAir, 'departure_datetime':DepDateTime, 'arrival_datetime':ArivalDateTime, 'duration':Flightdur}, status=status.HTTP_200_OK)

    else:
        # Error in seralising, print error
        print("ERROR")
        print(TheSerlizer.errors)
        return Response("The JSON payload is not valid", status=status.HTTP_503_SERVICE_UNAVAILABLE)
    # success, return response
    

#curl -H 'Content-Type: application/json' -X POST -d '{"booking_number":"XPP13AEAMP"}' http://127.0.0.1:8000/api/cancelbooking/
# 7) Cancel Booking - POST
@api_view(['POST'])
def cancelbooking(request):

    # Seralise the request data
    TheSerlizer = bookingNumSerializer(data=request.data)
    # check to see if the data was valid
    if TheSerlizer.is_valid():
        print("Valid stuff")

        # retrieve booking number from request data
        TheBookingNum = TheSerlizer.data['booking_number']

        # Retrive the booking object
        ThebookingObj = Booking.objects.filter(booking_number=TheBookingNum).first()

        # change the booking object status to canccelled
        ThebookingObj.status = "CANCELLED"

        # save the changes
        ThebookingObj.save()

        return Response({'booking_number':TheBookingNum, 'booking_status':ThebookingObj.status}, status=status.HTTP_201_CREATED)

    else:
        return Response("The JSON payload is not valid", status=status.HTTP_503_SERVICE_UNAVAILABLE)

